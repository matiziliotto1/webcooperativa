-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 16-03-2020 a las 20:36:58
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cooperativa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_comunicaciones`
--

CREATE TABLE `centro_comunicaciones` (
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `centro_comunicaciones`
--

INSERT INTO `centro_comunicaciones` (`descripcion`, `texto`, `ver`) VALUES
('subtitulo', 'Subtitulo', 1),
('texto_debajo_subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br />\r\n<br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br />\r\n<br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br />\r\n<br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', 1),
('texto_debajo_titulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.', 1),
('titulo_imagen', 'Titulo de la imagen', 1),
('titulo_seccion', 'Titulo de la seccion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `energia`
--

CREATE TABLE `energia` (
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `energia`
--

INSERT INTO `energia` (`descripcion`, `texto`, `ver`) VALUES
('subtitulo', 'Subtitulo', 1),
('texto_debajo_subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br />\r\n<br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br />\r\n<br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br />\r\n<br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', 1),
('texto_debajo_titulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.', 1),
('titulo_imagen', 'Titulo de la imagen', 1),
('titulo_seccion', 'Titulo de la seccion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `farmacia`
--

CREATE TABLE `farmacia` (
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `farmacia`
--

INSERT INTO `farmacia` (`descripcion`, `texto`, `ver`) VALUES
('subtitulo', 'Subtitulo', 1),
('texto_debajo_subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br />\r\n<br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br />\r\n<br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br />\r\n<br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', 1),
('texto_debajo_titulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.', 1),
('titulo_imagen', 'Titulo de la imagen', 1),
('titulo_seccion', 'Titulo de la seccion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gnc`
--

CREATE TABLE `gnc` (
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `gnc`
--

INSERT INTO `gnc` (`descripcion`, `texto`, `ver`) VALUES
('subtitulo', 'Subtitulo', 1),
('texto_debajo_subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.\r\n\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.\r\n\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.\r\n\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', 1),
('texto_debajo_titulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.', 1),
('titulo_imagen', 'Titulo de la imagen', 1),
('titulo_seccion', 'Titulo de la seccion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home`
--

CREATE TABLE `home` (
  `descripcion` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home`
--

INSERT INTO `home` (`descripcion`, `texto`, `ver`) VALUES
('primer_subtexto', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae? ', 1),
('seccion_cooperativa_subtexto', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae?<br />\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae? ', 1),
('seccion_cooperativa_titulo', 'Historia de la cooperativa', 1),
('subtexto_tramites_formularios', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate vero obcaecati natus adipisci necessitatibus eius, enim vel sit ad reiciendis. Enim praesentium magni delectus cum, tempore deserunt aliquid quaerat culpa nemo veritatis, iste adipisci excepturi consectetur doloribus aliquam accusantium beatae? ', 1),
('titulo_imagen', 'Brindamos servicios eficientes, sustentables y de calidad', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `label` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id_menu`, `label`, `link`, `orden`) VALUES
(1, 'INICIO', 'home.php', 1),
(2, 'SEPELIOS', 'sepelios.php', 2),
(5, 'GNC', 'gnc.php', 3),
(6, 'FARMACIA', 'farmacia.php', 4),
(9, 'CENTRO DE COMUNICACIONES', 'centro_comunicaciones.php', 5),
(10, 'ENERGIA', 'energia.php', 6),
(11, 'TRAMITES Y FORMULARIOS', 'tramites_formularios.php', 7),
(12, 'NOTICIAS', 'noticias.php', 8),
(13, 'CONTACTO', 'contacto.php', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL,
  `titulo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cuerpo` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `imagen` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id_noticia`, `titulo`, `cuerpo`, `fecha`, `imagen`) VALUES
(2, 'Titulo2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br /><br />\r\n<br /><br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br /><br />\r\n<br /><br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br /><br />\r\n<br /><br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', '2222-02-21', 'ceb_saneamiento.jpg'),
(3, 'Titulo3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br /><br />\r\n<br /><br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br /><br />\r\n<br /><br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br /><br />\r\n<br /><br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', '2020-02-20', 'tramites_y_formularios.jpeg'),
(4, 'Titulo4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br /><br />\r\n<br /><br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br /><br />\r\n<br /><br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br /><br />\r\n<br /><br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', '1212-12-12', 'ceb_saneamiento.jpg'),
(5, 'buadab', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br /><br />\r\n<br /><br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br /><br />\r\n<br /><br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br /><br />\r\n<br /><br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', '1232-03-12', 'ceb_saneamiento.jpg'),
(8, 'sadasd', 'hdsad', '2020-12-21', 'img_v_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sepelios`
--

CREATE TABLE `sepelios` (
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sepelios`
--

INSERT INTO `sepelios` (`descripcion`, `texto`, `ver`) VALUES
('subtitulo', 'Subtitulo', 1),
('texto_debajo_subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.<br />\r\n<br />\r\nFeugiat nibh sed pulvinar proin gravida hendrerit lectus a. In dictum non consectetur a erat. Nullam ac tortor vitae purus faucibus ornare. Quis auctor elit sed vulputate. Pretium fusce id velit ut tortor. Lacinia at quis risus sed vulputate odio. Pulvinar pellentesque habitant morbi tristique. Fermentum odio eu feugiat pretium nibh. Sed arcu non odio euismod lacinia at quis. Accumsan tortor posuere ac ut consequat semper. Ac turpis egestas sed tempus urna et. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Amet dictum sit amet justo donec. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Convallis tellus id interdum velit laoreet id donec ultrices. Viverra vitae congue eu consequat ac felis donec et odio. Mattis nunc sed blandit libero volutpat sed cras. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Pharetra massa massa ultricies mi quis hendrerit dolor.<br />\r\n<br />\r\nPretium lectus quam id leo in vitae turpis massa sed. Eget nulla facilisi etiam dignissim diam quis enim lobortis. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Et ultrices neque ornare aenean euismod elementum nisi quis. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Id aliquet lectus proin nibh nisl condimentum id venenatis a. Dolor morbi non arcu risus quis varius. Nisi porta lorem mollis aliquam ut porttitor. Ornare quam viverra orci sagittis eu volutpat odio. Mauris a diam maecenas sed enim. Velit scelerisque in dictum non consectetur a. Mauris ultrices eros in cursus turpis massa. Congue quisque egestas diam in arcu cursus euismod quis viverra. Congue quisque egestas diam in arcu cursus. Vel elit scelerisque mauris pellentesque pulvinar. Arcu ac tortor dignissim convallis aenean.<br />\r\n<br />\r\nCras fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Quam pellentesque nec nam aliquam sem et tortor consequat. Tortor condimentum lacinia quis vel eros donec. Amet est placerat in egestas erat imperdiet sed. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Egestas egestas fringilla phasellus faucibus. Fermentum et sollicitudin ac orci. Auctor elit sed vulputate mi sit amet mauris. Neque viverra justo nec ultrices dui sapien. Nunc sed blandit libero volutpat. Urna nunc id cursus metus aliquam eleifend mi in.', 1),
('texto_debajo_titulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris. Condimentum lacinia quis vel eros donec. Sit amet volutpat consequat mauris nunc congue nisi vitae. Nisl suscipit adipiscing bibendum est ultricies. Neque viverra justo nec ultrices dui. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. A erat nam at lectus urna duis. Libero justo laoreet sit amet cursus sit amet dictum sit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Sit amet nisl suscipit adipiscing. Amet justo donec enim diam vulputate ut pharetra sit amet. Morbi quis commodo odio aenean sed adipiscing diam. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Fermentum dui faucibus in ornare quam.', 1),
('titulo_imagen', 'Titulo de la imagen', 1),
('titulo_seccion', 'Titulo de la seccion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites_formularios`
--

CREATE TABLE `tramites_formularios` (
  `id_tramite` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `pdf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tramites_formularios`
--

INSERT INTO `tramites_formularios` (`id_tramite`, `titulo`, `descripcion`, `pdf`) VALUES
(2, '1titulo12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris.', 'transp02-procesos.pdf'),
(3, 'Tramite 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris.', 'transp01-introducción.pdf'),
(4, 'tatasad', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris.', 'transp01-introducción.pdf'),
(5, 'asdasd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mauris sit amet massa vitae tortor. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Quis hendrerit dolor magna eget est. Fames ac turpis egestas maecenas pharetra convallis posuere morbi leo. Quam viverra orci sagittis eu volutpat odio facilisis mauris.', 'Curso Introductorio de Linux.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellido`, `email`, `password`) VALUES
(1, 'Matias', 'Ziliotto', 'mati_97_ziliotto@hotmail.com', '$2y$10$RaWdGFbsedOgd.w7d0qqi.WTS8RWL7q9C.JaRPYMfKHN.r0EBUbs6'),
(2, 'Admin', 'Admin', 'admin@hotmail.com', '$2y$10$diVmDRcxLbO8Ii2XIMHzDuaQcIcItgovoehAKcJs0GfbUhXOsqkt2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `centro_comunicaciones`
--
ALTER TABLE `centro_comunicaciones`
  ADD PRIMARY KEY (`descripcion`);

--
-- Indices de la tabla `energia`
--
ALTER TABLE `energia`
  ADD PRIMARY KEY (`descripcion`);

--
-- Indices de la tabla `farmacia`
--
ALTER TABLE `farmacia`
  ADD PRIMARY KEY (`descripcion`);

--
-- Indices de la tabla `gnc`
--
ALTER TABLE `gnc`
  ADD PRIMARY KEY (`descripcion`);

--
-- Indices de la tabla `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`descripcion`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noticia`);

--
-- Indices de la tabla `sepelios`
--
ALTER TABLE `sepelios`
  ADD PRIMARY KEY (`descripcion`);

--
-- Indices de la tabla `tramites_formularios`
--
ALTER TABLE `tramites_formularios`
  ADD PRIMARY KEY (`id_tramite`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tramites_formularios`
--
ALTER TABLE `tramites_formularios`
  MODIFY `id_tramite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
