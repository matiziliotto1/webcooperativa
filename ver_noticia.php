<?php
  include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; Noticias</title>
<?php
    $id_noticia = $_REQUEST['id_noticia'];

    $instruccion = "select * from noticias where id_noticia=".$id_noticia.";";
    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");
    $fila = mysqli_fetch_array($consulta);

    $titulo = $fila['titulo'];
    $cuerpo = $fila['cuerpo'];
    $fecha = $fila['fecha'];

    setlocale(LC_TIME, 'es_ES', 'esp_esp'); 
    $año = substr($fila['fecha'],0,4); 
    $mes = substr($fila['fecha'],5,2); 
    $dia = substr($fila['fecha'],8,2);
    
    $mes = ucfirst(strftime("%B", mktime(0, 0, 0, $mes)));

    $fecha = $dia." de ".$mes." ".$año;

    $imagen = $fila['imagen'];

    mysqli_close($conexion);
?>

<div class="site-section py-0 m-0">
    <div class="owl-carousel hero-slide">
        <div class="site-section">
            <div class="">
                <div class="half-post-entry d-block d-lg-flex bg-light" style="height: 35%">
                    <div style="position: relative;display: inline-block;">
                        <img src="images/imagen_noticias_fondo.jpg" width="100%">
                        <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 30%;top: 50%;width: 60%;">
                            <h1>Noticias</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="site-section" style="background-color: #F2F2F2;">
    <div class="container bg-white p-5">
        <div class="col-lg-12 single-content">
            <h1 class="mb-4 color-titulos" style="text-align:center;">
                <?php echo $titulo; ?>
            </h1>
            <p style="text-align: right;">
                Creado: <?php echo $fecha; ?>
            </p>
            <p class="mb-5">
                <?php echo '<img class="img-fluid" src="images/noticias/'.$imagen.'">'; ?>
                
            </p>  
                <?php echo $cuerpo; ?>
            <p>

            </p>
        </div>    
    </div>
</div>

<?php
    include_once('footer.php');
?>