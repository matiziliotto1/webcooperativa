<?php
  include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; Noticias</title>
<style>
    .btn-primary{
        border-radius: 5px;
        background-color: #0c4346;
        cursor: pointer;
        color: white;
        margin-top: 2%;
        border: none;
        width: auto;
        padding: 1%;
        padding-left: 3%;
        padding-right: 3%;
        -webkit-box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.45);
        -moz-box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.45);
        box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.45);
        justify-content: center;
    }
    .btn-primary:hover{
        background-color: #44C5CA;
        color: black;
    }
</style>
<div class="site-section py-0 m-0">
    <div class="owl-carousel hero-slide">
        <div class="site-section">
            <div class="">
                <div class="half-post-entry d-block d-lg-flex bg-light" style="height: 35%">
                    <div style="position: relative;display: inline-block;">
                        <img src="images/imagen_noticias_fondo.jpg" width="100%">
                        <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 30%;top: 50%;width: 60%;">
                            <h1>Noticias</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-section" style="background-color: #F2F2F2;">
    <div class="container bg-white p-5">
        <h2 class="color-titulos">
            Noticias
        </h2>
        <?php

            if (isset($_GET['pagina'])) {
                $pagina = $_GET['pagina'];
            } else {
                $pagina = 1;
            }
            $noticias_x_pagina = 20;
            $offset = ($pagina-1) * $noticias_x_pagina;

            $paginas_totales = "select count(*) from noticias";
            $resultado = mysqli_query($conexion,$paginas_totales);

            $nfilas = mysqli_fetch_array($resultado)[0];
            $paginas_totales = ceil($nfilas / $noticias_x_pagina);

            $instruccion = "select * from noticias order by fecha limit ".$offset.",".$noticias_x_pagina.";";

            $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar noticias");

            $nfilas = mysqli_num_rows($consulta);
            if ($nfilas > 0) {
                for ($i=0; $i<$nfilas; $i++) {
                    $fila = mysqli_fetch_array($consulta);

                    setlocale(LC_TIME, 'es_ES', 'esp_esp'); 
                    $año = substr($fila['fecha'],0,4); 
                    $mes = substr($fila['fecha'],5,2); 
                    $dia = substr($fila['fecha'],8,2);
                    
                    $mes = ucfirst(strftime("%B", mktime(0, 0, 0, $mes)));

                    $fecha = $dia." de ".$mes." ".$año;

                    print(
                        '<div class="post-entry-2 d-flex p-3" style="-webkit-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                        -moz-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                        box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);">
                        <div class="thumbnail order-md-2" style="background-image: url('."'images/noticias/".$fila['imagen']."'".')"></div>
                        <div class="contents order-md-1 pl-0">
                            <h4><a href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'" style="color: #2c2c2c;">'.$fila['titulo'].'</a></h4>
                            <p class="mb-3">'.substr($fila['cuerpo'], 0, 250).'...</p>
                            <div class="post-meta">
                            <span class="d-block"><a class="color-links" href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'">Ver noticia</a></span>
                            <span class="date-read">'.$fecha.'<span class="mx-1">
                            </div>
                        </div>
                        </div>'
                    );
                }
                
            }
            mysqli_close($conexion);
        ?>

        <div id="nuevasNoticias">
            <input type="hidden" id="pagina" value=1>
            <input type="hidden" id="ultima_pagina" value=<?php echo $paginas_totales; ?>>
        </div>

        <div id="fin_noticias" class="p-2" style="display:none;">
            <h5>No hay mas noticias para cargar</h5>
        </div>

        <div class="btn btn-primary">
            <a id="masNoticias" style="margin-bottom:3%;">Ver mas noticias</a>
        </div>
      
    </div>
</div>
<?php
    include_once('footer.php');
?>

<script>
	$("#masNoticias").click(function(){
	    page=parseFloat($("#pagina").val());

	    if(page<parseFloat($("#ultima_pagina").val())){
	    	page++
		    $("#pagina").val(page);
            ;

		    var params="pagina="+page;

		    $.ajax({
		        url: "/Proyectos/webcooperativa/noticiasAJAX.php?"+params,
		      }).done(function(data) {
		        $( "#nuevasNoticias" ).append( data );
		    });
		}
		else{
			document.getElementById("fin_noticias").style.display="block";
		}
	})
</script>