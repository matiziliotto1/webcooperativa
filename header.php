<?php
    include("admin/configDB.php");
    //TODO: ESTA LINEA ESTA PUESTA EN EL SERVIDOR PARA QUE ANDE EL MKTIME DE HOME Y NOTICIAS
    //date_default_timezone_set("America/Argentina/Salta");
?>
<head>
    <meta name="author" content="Ziliotto Matias y Miño Lopez Jorge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css?family=B612+Mono|Cabin:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">
    <link href="css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/logo.png" type="image/jpg">

    <style type="text/css">
    .titulo_novedades{
        color:#FAFAFA;
        text-align: center;
        font-family: 'Signika',sans-serif !important;
        font-weight: 400;
        padding-top: 10%;
    }
    .tabla_novedades{
        text-align: right;
        padding-right: 3%;
        padding-top: 3%;
    }
    .search-form .btn{
        background-color: #0c4346;
        border-color: #0c4346;
    }
    .search-form .btn:hover{
        background-color: #44C5CA;
        border-color: #44C5CA;
    }
        @media (min-width: 768px){
            .site-section{
            padding: 2.5em 0;
            }
            .titulo-img{
            top:20%;
            }
        }
        @media (max-width:1000px){
            .site-section{
            padding: 0;
            }
            .titulo-img{
            top:10%!important;
            }

            .site-section > .row{
                margin-left: 0;
                margin-right: 0;
            }
            .thumbnail{
                display:none;
            }
            .site-nav-wrap > li > .nav-link{
                border-bottom: 2px solid #0c4346;
            }
        }
        @media (max-width:701px){
            .titulo-img h1{
                font-size: 6vw;
            }
            .titulo_novedades{
                font-size: 7vw;
            }
            .tabla_novedades h4{
                font-size: 5vw;
            }
        }

        @media (max-width: 1000px) and (min-width: 700px) {
            .nav-link{
                padding: 0.5rem 0.4rem!important;
            }
        }
        .color-links{
            color:#339498!important;
        }
        .color-titulos{
            color:#0c4346!important;
        }
        .img-superpuesta{
            position: absolute;
            z-index: 999;
            margin: 0 auto;
            right: 0;
        }
    </style>
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    <div class="site-wrap">
        <div class="site-mobile-menu site-navbar-target" style="background-color: #F7F7F7;box-shadow: rgb(0, 0, 0) 0px 0px 20px -10px inset;">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>
        <div class="header-top" style="background-color: #F7F7F7;-webkit-box-shadow: inset 0px 0px 20px -10px rgba(0,0,0,1);-moz-box-shadow: inset 0px 0px 20px -10px rgba(0,0,0,1);box-shadow: inset 0px 0px 20px -10px rgba(0,0,0,1);">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6 d-flex">
                        <a href="home.php" class="site-logo">
                            <img src="images/logo.jpg" width="100" height="60">
                        </a>
                        <a href="#" class="ml-auto d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                        class="icon-menu h3"></span></a>
                    </div>
                    <div class="col-12 col-lg-6 ml-auto d-flex">
                        <div class="ml-md-auto top-social d-none d-lg-inline-block">
                            <a href="#" class="d-inline-block p-3"><span class="icon-facebook"></span></a>
                            <a href="#" class="d-inline-block p-3"><span class="icon-twitter"></span></a>
                            <a href="#" class="d-inline-block p-3"><span class="icon-instagram"></span></a>
                        </div>
                        <form action="resultados.php" class="search-form d-inline-block" method="get">
                            <div class="d-flex">
                                <input type="text" class="form-control" name="buscar" placeholder="Buscar..." required>
                                <button type="submit" class="btn btn-secondary" ><span class="icon-search"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="site-navbar js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner" style="padding:1%;background-color: #44c5ca;-webkit-box-shadow: inset 0px 0px 20px -10px rgba(0,0,0,1);-moz-box-shadow: inset 0px 0px 20px -10px rgba(0,0,0,1);box-shadow: inset 0px 0px 20px -10px rgba(0,0,0,1);">
                <div class="container">
                    <div class="d-flex align-items-center">

                        <div class="mr-auto">
                            <nav class="site-navigation position-relative text-right" role="navigation">
                                <ul class="site-menu main-menu js-clone-nav mr-auto d-none pl-0 d-lg-block">
                                    <?php
                                    
                                    $instruccion = "select * from menu ORDER BY orden;";
                                    
                                    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

                                    $nfilas = mysqli_num_rows($consulta);

                                    if ($nfilas > 0) {
                                        for ($i=0; $i<$nfilas; $i++) {
                                            $fila = mysqli_fetch_array($consulta);
                                            if ($_SERVER['REQUEST_URI']=="/"."Proyectos/"."webcooperativa/".$fila['link']) {
                                                print('<li class="active"><a href="'.$fila['link'].'" class="nav-link" style="color:#fafafa">');
                                            } else {
                                                print('<li><a href="'.$fila['link'].'" class="nav-link">');
                                            }
                                            print($fila['label'].'</a></li>');
                                        }
                                    }
                                    mysqli_close($conexion);
                                    
                                    ?>
                                </ul>                                                                                                                                                                                                                                                                                         
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="position:fixed;bottom:15;right:15;z-index: 1000;">
            <a href="contacto.php">
                <img src="images/radio.png" width=55 height=55>
            </a>
        </div>
       