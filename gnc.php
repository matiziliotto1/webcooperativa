<?php
    include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; GNC</title>
<style>
    @media (max-width:1000px){
        .img-superpuesta{
            left:70%!important;
            width:20%!important;
        }
    }
</style>
<?php
$instruccion = "select * from gnc;";
$consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

$nfilas = mysqli_num_rows($consulta);

if ($nfilas > 0) {
    for ($i=0; $i<$nfilas; $i++) {
        $fila = mysqli_fetch_array($consulta);

        switch ($fila['descripcion']) {
            case 'titulo_imagen':
                $titulo_imagen = new stdClass();
                $titulo_imagen->texto = $fila['texto'];
                $titulo_imagen->mostrar = $fila['ver'];
                break;
            case 'titulo_seccion':
                $titulo_seccion = new stdClass();
                $titulo_seccion->texto = $fila['texto'];
                $titulo_seccion->mostrar = $fila['ver'];
                break;
            case 'texto_debajo_titulo':
                $texto_debajo_titulo = new stdClass();
                $texto_debajo_titulo->texto = $fila['texto'];
                $texto_debajo_titulo->mostrar = $fila['ver'];
                break;
            case 'subtitulo':
                $subtitulo = new stdClass();
                $subtitulo->texto = $fila['texto'];
                $subtitulo->mostrar = $fila['ver'];
                break;
            case 'texto_debajo_subtitulo':
                $texto_debajo_subtitulo = new stdClass();
                $texto_debajo_subtitulo->texto = $fila['texto'];
                $texto_debajo_subtitulo->mostrar = $fila['ver'];
                break;
            default:
                break;
        }
    }
}
  mysqli_close($conexion);
?>
    <div class="site-section py-0 m-0">
        <div class="owl-carousel hero-slide">
            <div class="site-section">
                <div class="">
                    <div class="half-post-entry d-block d-lg-flex bg-light" style="height: 35%">
                        <div style="position: relative;display: inline-block;">
                            <img src="images/gnc.jpg">
                            <img class="img-superpuesta" src="images/gnc.png" style="left:80%;top: 50px;width: 12%;">
                            <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 30%;top: 50%;width: 60%;">
                                <h1>
                                    <?php
                                    if ($titulo_imagen->mostrar) {
                                        echo $titulo_imagen->texto;
                                    }
                                    ?>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section" style="background-color: #F2F2F2;">
        <div class="container bg-white p-5">
            <h2 class="color-titulos">
                <?php
                if ($titulo_seccion->mostrar) {
                    echo $titulo_seccion->texto;
                }
                ?>
            </h2>
            <p>
                <?php
                if ($texto_debajo_titulo->mostrar) {
                    echo $texto_debajo_titulo->texto;
                }
                ?>
            </p>
            <h4 class="mt-5 mb-2 color-titulos">
                <?php
                if ($subtitulo->mostrar) {
                    echo $subtitulo->texto;
                }
                ?>
            </h4>
            <p>
                <?php
                if ($texto_debajo_subtitulo->mostrar) {
                    echo $texto_debajo_subtitulo->texto;
                }
                ?>
            </p>
        </div>
    </div>
<?php
    include_once('footer.php');
?>