<?php
  session_start();
?>
<?php
  include("../configDB.php");
?>    
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cooperativa &mdash; Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="icon" href="../../images/logo.png" type="image/jpg">

</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(../../images/logo.jpg);">
					<span class="login100-form-title-1">
						Administracion
					</span>
        </div>
        <form class="login100-form validate-form" method="POST" enctype="multipart/form-data" action="login.php"> 

<?php
  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form 
    
    $myemail=$_POST['email'];
    $mypassword=$_POST['password'];
    
    $sql = "select * from usuarios where email='$myemail';";

    $result = mysqli_query($conexion,$sql) or die ("Fallo en la consulta");
    
    $count = mysqli_num_rows($result);

	$result= mysqli_fetch_array($result);

	if($count == 1 && password_verify($mypassword, $result['password'])) {
	$_SESSION['usuario_valido']="si";
	$_SESSION['usuario'] = $result['nombre']." ".$result['apellido'];
	$_SESSION['id_usuario'] = $result['id_usuario'];
	//echo "<script> window.location.replace('/index.php') </script>";
	header("location:  /Proyectos/webcooperativa/admin/home/");
	} else {
		error();
	}
  }

  //La funcion era para ver si la contraseña estaba mal.. que tire un error al logearse
  function error()
  {
    print("<div class='alert alert-danger alert-dismissible fade show' role='alert'>
            El e-mail o contraseña es incorrecto
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>");
  }

  mysqli_close($conexion);
?>

					<div class="wrap-input100 validate-input m-b-26" data-validate="Ingresar el e-mail es obligatorio">
						<span class="label-input100">E-mail</span>
						<input class="input100" type="email" name="email" placeholder="Ingrese su e-mail" required>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Ingresar la contraseña es obligatorio">
						<span class="label-input100">Contraseña</span>
						<input class="input100" type="password" name="password" placeholder="Ingresa la contraseña" required>
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Recordarme
							</label>
						</div>
  					<!--
						<div>
							<a href="#" class="txt1">
								Olvidaste tu contraseña?
							</a>
						</div>
					-->
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Ingresar
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>