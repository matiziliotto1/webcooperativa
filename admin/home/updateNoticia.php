<?php
    include("../configDB.php");
    include_once('chequear_admin.php');
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id_noticia = $_POST['id_noticia'];
    $new_titulo = $_POST['titulo'];
    $new_cuerpo = $_POST['cuerpo'];
    $new_fecha = $_POST['fecha'];
    $new_imagen = $_FILES['img_dest']['name'];
    $instruccion = "";
    if ($new_imagen) {
        ini_set('upload-max-filesize', '10M');
        ini_set('post_max_size', '10M');
        subirFoto($id_noticia);
        $instruccion = "update noticias set titulo='".$new_titulo."',cuerpo='".nl2br($new_cuerpo)."',fecha='".$new_fecha."',imagen='".$id_noticia.$new_imagen."' where id_noticia=".$id_noticia.";";
    } else {
        $instruccion = "update noticias set titulo='".$new_titulo."',cuerpo='".nl2br($new_cuerpo)."',fecha='".$new_fecha."' where id_noticia=".$id_noticia.";";
    }
    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

    echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/editarNoticia.php?id_noticia=".$id_noticia."&noticia_editada=1') </script>";
    mysqli_close($conexion);
}
?>

<?php
function subirFoto($id_noticia)
{
    $message = '';
    // get details of the uploaded file
    $fileTmpPath = $_FILES['img_dest']['tmp_name'];
    $fileName = $_FILES['img_dest']['name'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    // check if file has one of the following extensions
    $allowedfileExtensions = array('jpg', 'png','jpeg');
    if (in_array($fileExtension, $allowedfileExtensions)) {
        // directory in which the uploaded file will be moved
        $uploadFileDir = '../../images/noticias/';
        $dest_path = $uploadFileDir . $id_noticia . $fileName;
        if (move_uploaded_file($fileTmpPath, $dest_path)) {
            $message ='Imagen subida correctamente.';
        } else {
            $message = 'Hubo un problema subiendo la imagen al servidor.';
        }
    } else {
        $message = 'Subida fallida. Tipos de imagen permitidos: ' . implode(',', $allowedfileExtensions);
    }
}
?>
