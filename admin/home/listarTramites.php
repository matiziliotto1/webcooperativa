<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<style>
    .card-body{
        padding:0.5rem;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Tramites y formularios: ver todos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="crearTramite.php" style="margin:auto;text-align:center;">Crear nuevo</a></h1>
    <div class="d-sm-flex align-items-center justify-content-between mb-4 pt-2">
        
        <div class="row" style="margin:auto;justify-content:left;">
            <?php
                $instruccion = "select * from tramites_formularios;";
        
                $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

                $nfilas = mysqli_num_rows($consulta);

                if ($nfilas > 0) {
                    for ($i=0; $i<$nfilas; $i++) {
                        $fila = mysqli_fetch_array($consulta);

                        echo "
                        <div class='col-md-4 mb-4'>
                            <div class='card border-left-primary shadow h-100 py-2'>
                                <div class='card-body'>
                                    <div class='row no-gutters align-items-center'>
                                        <div class='col mr-2'>
                                            <div class='h5 mb-2 font-weight-bold text-gray-800'>".$fila['titulo']."</div>
                                            <div class='h6 mb-3 text-gray-800'>".$fila['descripcion']."</div>
                                        </div>
                                    </div>
                                    <div class='col-auto'>
                                        <div class='h5 mb-0 text-gray-800'>PDF adjunto: ".$fila['pdf']."</div>
                                    </div>
                                    
                                </div>
                                <button class='btn btn-primary' style='width:auto;margin:auto;margin-bottom:2%;' onclick='redireccionarEditar(".$fila['id_tramite'].")'>Editar</button>
                                <button class='btn btn-primary' style='width:auto;margin:auto;' data-toggle='modal' data-target='#eliminarTramiteModal".$fila['id_tramite']."'>Eliminar</button>
                            </div>
                        </div>
                        ";
                        echo "

                            <div class='modal fade' id='eliminarTramiteModal".$fila['id_tramite']."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                <div class='modal-dialog' role='document'>
                                <div class='modal-content'>
                                    <div class='modal-header'>
                                    <h5 class='modal-title' id='exampleModalLabel'>Estas seguro de eliminar el tramite?</h5>
                                    <button class='close' type='button' data-dismiss='modal' aria-label='Close'>
                                        <span aria-hidden='true'>×</span>
                                    </button>
                                    </div>
                                    <div class='modal-body'>Si estas seguro de eliminar el tramite, selecciona 'eliminar'</div>
                                    <div class='modal-footer'>
                                    <button class='btn btn-secondary' type='button' data-dismiss='modal'>Cancelar</button>
                                    <a class='btn btn-primary' href='#' onclick='redireccionarEliminar(".$fila['id_tramite'].")'>Eliminar</a>
                                    <form method='POST' action='eliminarTramite.php' id='form".$fila['id_tramite']."'>
                                        <input type='hidden' name='id_tramite' value='".$fila['id_tramite']."'>
                                    </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                            ";
                    }
                }
                mysqli_close ($conexion);
            ?>
        </div>
        
    </div>
    <?php
            if ($_REQUEST['tramite_eliminado']==1) {
            error();
            }
            function error(){

            echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                    Tramite eliminado correctamente
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    </div>";
            }
        ?>
</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>

<script>
    function redireccionarEditar(id_tramite){
        window.location.href="editarTramite.php?id_tramite="+id_tramite;
    }
    function redireccionarEliminar(id_tramite){
        //window.location.href="eliminarTramite.php?id_tramite="+id_tramite;
        document.forms['form'+id_tramite].submit();
    }
</script>