<?php
	session_start();
?>
<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
	if($_SERVER["REQUEST_METHOD"] == "POST") {

		$instruccion = "select password from usuarios where id_usuario=".$_SESSION['id_usuario'].";";

		$resultado = mysqli_query($conexion,$instruccion) or die ("Fallo en la consulta");

		$resultado = mysqli_fetch_array ($resultado);

		$old_password = $resultado['password'];

		$old_password_form = $_POST['old_password'];

		$new_password = password_hash($_POST['new_password'], PASSWORD_DEFAULT);

		if(password_verify($old_password_form, $old_password)){
			$instruccion = "update usuarios set password='$new_password' where id_usuario=".$_SESSION['id_usuario'].";";

			$resultado = mysqli_query($conexion,$instruccion) or die ("Fallo en la consulta");
        	echo "<script> window.location.replace('perfil.php?perfil_editado=1') </script>";
			//header("location:  /Proyectos/AltaCostura/perfil.php?actualizado=1");
		}
		else{
			$error=true;
		}

		mysqli_close($conexion);
	}
?>

<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<h1 class="h3 mb-0 text-gray-800">Cambiar contraseña</h1>
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<div class="container">
			<form method="POST" action="editarPassword.php" enctype="multipart/form-data" style="text-align:left;padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form-edit-password">
				<div class="form-group">
					<label for="old_password">Contraseña actual</label>
					<input id="password" type="password" class="form-control" name="old_password" placeholder="Ingrese la contraseña actual"  required autocomplete="password" autofocus>
				</div>

				<div class="form-group">
					<label for="password">Contraseña nueva</label>	        
					<input id="new_password" type="password" class="form-control" name="new_password" placeholder="Ingrese la contraseña nueva" required autocomplete="current-password">
				</div>

				<div class="form-group">
					<label for="password">Confirmar contraseña</label>
					<input id="confirm_password" type="password" class="form-control" name="confirm_password" placeholder="Confirme la contraseña nueva" required autocomplete="current-password">
				</div>

				<!-- Boton que acciona las tareas para cambiar de contraseña -->
				<button type="submit" class="btn btn-primary">Guardar contraseña</button>
			</form>
			<code>
				<?php
					if(isset($error)){
						print("<div style='margin-top:3%;' class='alert alert-danger alert-dismissible fade show mx-auto' role='alert'>
						La contraseña ingresada no coincide con la actual
						<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
						</button>
					</div>");
					}	
				?>
			</code>

			<div class="alert alert-danger alert-dismissible fade show mx-auto" style="display: none;margin-top:3%;" role="alert" id="error">
				Las nuevas contraseñas ingresadas no coinciden
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
		
		</div>  
  </div>

</div>
<!-- /.container-fluid -->

<?php
   include_once('panelFin.php');
?>

<script>
	$("#form-edit-password").submit(function(e){
		console.log("aca entro");
		if(document.getElementById("new_password").value!=document.getElementById("confirm_password").value){
            e.preventDefault();
            document.getElementById("error").style.display = "block";
		}
	});
</script>