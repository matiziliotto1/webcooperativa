<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $titulo_imagen_aux = $_POST['titulo_imagen'];
    if ($_POST['titulo_imagen_mostrar']=='on') {
        $titulo_imagen_aux_mostrar = 1;
    } else {
        $titulo_imagen_aux_mostrar = 0;
    }
    $titulo_seccion_aux = $_POST['titulo_seccion'];
    if ($_POST['titulo_seccion_mostrar']=='on') {
        $titulo_seccion_aux_mostrar = 1;
    } else {
        $titulo_seccion_aux_mostrar = 0;
    }
    $texto_debajo_titulo_aux = $_POST['texto_debajo_titulo'];
    if ($_POST['texto_debajo_titulo_mostrar']=='on') {
        $texto_debajo_titulo_aux_mostrar = 1;
    } else {
        $texto_debajo_titulo_aux_mostrar = 0;
    }
    $subtitulo_aux = $_POST['subtitulo'];
    if ($_POST['subtitulo_mostrar']=='on') {
        $subtitulo_aux_mostrar = 1;
    } else {
        $subtitulo_aux_mostrar = 0;
    }
    $texto_debajo_subtitulo_aux = $_POST['texto_debajo_subtitulo'];
    if ($_POST['texto_debajo_subtitulo_mostrar']=='on') {
        $texto_debajo_subtitulo_aux_mostrar = 1;
    } else {
        $texto_debajo_subtitulo_aux_mostrar = 0;
    }
    
    $instruccion1 = "update sepelios set texto='".$titulo_imagen_aux."', ver='".$titulo_imagen_aux_mostrar."' where descripcion='titulo_imagen'";
    $consulta1 = mysqli_query($conexion, $instruccion1) or die("Fallo en la consulta1");

    $instruccion2 = "update sepelios set texto='".$titulo_seccion_aux."', ver='".$titulo_seccion_aux_mostrar."' where descripcion='titulo_seccion'";
    $consulta2 = mysqli_query($conexion, $instruccion2) or die("Fallo en la consulta2");

    $instruccion3 = "update sepelios set texto='".nl2br($texto_debajo_titulo_aux)."', ver='".$texto_debajo_titulo_aux_mostrar."' where descripcion='texto_debajo_titulo'";
    $consulta3 = mysqli_query($conexion, $instruccion3) or die("Fallo en la consulta3");

    $instruccion4 = "update sepelios set texto='".$subtitulo_aux."', ver='".$subtitulo_aux_mostrar."' where descripcion='subtitulo'";
    $consulta4 = mysqli_query($conexion, $instruccion4) or die("Fallo en la consulta4");

    $instruccion5 = "update sepelios set texto='".nl2br($texto_debajo_subtitulo_aux)."', ver='".$texto_debajo_subtitulo_aux_mostrar."' where descripcion='texto_debajo_subtitulo'";
    $consulta5 = mysqli_query($conexion, $instruccion5) or die("Fallo en la consulta5");
    
    //header("location: /Proyectos/WebCooperativa/admin/home/crearNoticia.php?noticia_creada=1");
    echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/sepelios.php?sepelios_editado=1') </script>";
}
?>

<?php
$instruccion = "select * from sepelios;";
$consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

$nfilas = mysqli_num_rows($consulta);

if ($nfilas > 0) {
    for ($i=0; $i<$nfilas; $i++) {
        $fila = mysqli_fetch_array($consulta);
        
        switch ($fila['descripcion']) {
            case 'titulo_imagen':
                $titulo_imagen = new stdClass();
                $titulo_imagen->texto = $fila['texto'];
                $titulo_imagen->mostrar = $fila['ver'];
                break;
            case 'titulo_seccion':
                $titulo_seccion = new stdClass();
                $titulo_seccion->texto = $fila['texto'];
                $titulo_seccion->mostrar = $fila['ver'];
                break;
            case 'texto_debajo_titulo':
                $texto_debajo_titulo = new stdClass();
                $texto_debajo_titulo->texto = $fila['texto'];
                $texto_debajo_titulo->mostrar = $fila['ver'];
                break;
            case 'subtitulo':
                $subtitulo = new stdClass();
                $subtitulo->texto = $fila['texto'];
                $subtitulo->mostrar = $fila['ver'];
                break;
            case 'texto_debajo_subtitulo':
                $texto_debajo_subtitulo = new stdClass();
                $texto_debajo_subtitulo->texto = $fila['texto'];
                $texto_debajo_subtitulo->mostrar = $fila['ver'];
                break;
            default:
                break;
        }
    }
}
mysqli_close($conexion);
?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-0 text-gray-800">Sepelios</h1>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div class="container">
      <form method="POST" enctype="multipart/form-data" style="text-align:left;padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
          <div class="form-group form-width">
            <label>Titulo de la imagen</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="titulo_imagen_mostrar" <?php if ($titulo_imagen->mostrar) {echo 'checked';}?>>
                </div>
              </div>
                <input type="text" name="titulo_imagen" class="form-control input-form" placeholder="Ingrese el titulo de la imagen" required value="<?php echo $titulo_imagen->texto; ?>">
            </div>
          </div>
          <div class="form-group form-width">
            <label>Titulo de la seccion</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="titulo_seccion_mostrar" <?php if ($titulo_seccion->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <input type="text" name="titulo_seccion" class="form-control" placeholder="Ingrese el titulo de la seccion" required value="<?php echo $titulo_seccion->texto; ?>">
            </div>
          </div>
          <div class="form-group form-width">
            <label>Texto debajo del titulo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="texto_debajo_titulo_mostrar" <?php if ($texto_debajo_titulo->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <textarea name="texto_debajo_titulo" class="form-control" rows="5" placeholder="Texto debajo del titulo..."><?php echo strip_tags($texto_debajo_titulo->texto); ?></textarea>
            </div>
          </div>
          <div class="form-group form-width">
            <label>Subtitulo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="subtitulo_mostrar" <?php if ($subtitulo->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <input type="text" name="subtitulo" class="form-control" placeholder="Ingrese el subtitulo" required value="<?php echo $subtitulo->texto; ?>">
            </div>
          </div>
          <div class="form-group form-width">
            <label>Texto debajo del subtitulo</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="texto_debajo_subtitulo_mostrar" <?php if ($texto_debajo_subtitulo->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <textarea name="texto_debajo_subtitulo" class="form-control" rows="5" placeholder="Texto debajo del subtitulo..."><?php echo strip_tags($texto_debajo_subtitulo->texto); ?></textarea>
            </div>
          </div>

          <input type="submit" value="Actualizar" class="btn btn-primary">
          
      </form>
      <?php
        if ($_REQUEST['sepelios_editado']==1) {
            error();
        }
        function error()
        {
            echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                    Sección sepelios editada correctamente
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </div>";
        }
        ?>
    </div>  
  </div>

</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>