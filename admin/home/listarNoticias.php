<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<style>
    .card-body{
        padding:0.5rem;
    }
    .page-item.active .page-link{
        background-color: #44a9ad;
        border-color: #44a9ad;
    }
    @media (max-width:1000px){
        .col-4{
            min-width:50%;
        }
    }
    @media (max-width:701px){
        .col-4{
            min-width:100%;
        }
    }
    @media (max-width:501px){
        .h5{
            font-size: 100%;
        }
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div>
        <h1 class="h3 mb-0 text-gray-800">Noticias: ver todas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="crearNoticia.php" style="margin:auto;text-align:center;">Crear nueva</a></h1>
        
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4 pt-2">
        

        <!-- Aca deberiamos listar todas las noticias tipo tabla o en tarjetas para que aparezcan dos iconos de editar o eliminar
        y que al ingresar lo lleve a los php que hacen esas cosas-->
        <div class="row" style="margin:auto;justify-content:left;">
            <?php
                if (isset($_GET['pagina'])) {
                    $pagina = $_GET['pagina'];
                } else {
                    $pagina = 1;
                }
                $noticias_x_pagina = 20;
                $offset = ($pagina-1) * $noticias_x_pagina;

                $paginas_totales = "select count(*) from noticias";
                $resultado = mysqli_query($conexion,$paginas_totales) or die ("Fallo en la consulta");
                $total_filas = mysqli_fetch_array($resultado)[0];
                $paginas_totales = ceil($total_filas / $noticias_x_pagina);

                $instruccion = "select * from noticias order by fecha desc limit $offset, $noticias_x_pagina";
                $consulta = mysqli_query($conexion,$instruccion) or die ("Fallo en la consulta");

                $nfilas = mysqli_num_rows($consulta);

                if ($nfilas > 0) {
                    for ($i=0; $i<$nfilas; $i++) {
                        $fila = mysqli_fetch_array($consulta);

                        setlocale(LC_TIME, 'es_ES', 'esp_esp');
                        $año = substr($fila['fecha'], 0, 4);
                        $mes = substr($fila['fecha'], 5, 2);
                        $dia = substr($fila['fecha'], 8, 2);
                        
                        $fecha = $dia."/".$mes."/".$año;

                        echo "
                        <div class='col-4 mb-4 pr-3'>
                            <div class='card border-left-primary shadow h-100 py-2'>
                                <div class='card-body'>
                                    <div class='row no-gutters align-items-center'>
                                        <div class='col mr-2'>
                                            <div class='text-xs font-weight-bold text-primary text-uppercase mb-1'>".$fecha."</div>
                                            <div class='h5 mb-0 font-weight-bold text-gray-800'>".$fila['titulo']."</div>
                                        </div>
                                    </div>
                                    <div class='col-auto'>
                                        <img class='card-img-top' src='../../images/noticias/".$fila['imagen']."' alt='La imagen no se encontro'>
                                    </div>
                                    
                                </div>
                                <button class='btn btn-primary' style='width:auto;margin:auto;margin-bottom:2%;' onclick='redireccionarEditar(".$fila['id_noticia'].")'>Editar</button>
                                <button class='btn btn-primary' style='width:auto;margin:auto;' data-toggle='modal' data-target='#eliminarNoticiaModal".$fila['id_noticia']."'>Eliminar</button>
                            </div>
                        </div>
                        ";
                        echo "

                            <div class='modal fade' id='eliminarNoticiaModal".$fila['id_noticia']."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                <div class='modal-dialog' role='document'>
                                <div class='modal-content'>
                                    <div class='modal-header'>
                                    <h5 class='modal-title' id='exampleModalLabel'>Estas seguro de eliminar la noticia?</h5>
                                    <button class='close' type='button' data-dismiss='modal' aria-label='Close'>
                                        <span aria-hidden='true'>×</span>
                                    </button>
                                    </div>
                                    <div class='modal-body'>Si estas seguro de eliminar la noticia, selecciona 'eliminar'</div>
                                    <div class='modal-footer'>
                                    <button class='btn btn-secondary' type='button' data-dismiss='modal'>Cancelar</button>
                                    <a class='btn btn-primary' href='#' onclick='redireccionarEliminar(".$fila['id_noticia'].")'>Eliminar</a>
                                    <form method='POST' action='eliminarNoticia.php' id='form".$fila['id_noticia']."'>
                                        <input type='hidden' name='id_noticia' value='".$fila['id_noticia']."'>
                                    </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                            ";
                    }
                }
                mysqli_close ($conexion);
            ?>
        </div>
        
    </div>


    <ul class="pagination">
        <li class="page-item <?php if($pagina==1){ echo "disabled"; } ?>">
            <a class="page-link" href="?pagina=<?php echo $pagina-1; ?>" >Anterior</a>
        </li>
        <li class="page-item <?php if($pagina==1){ echo "active"; } ?>">
            <a class="page-link" href="?pagina=1" disabled>1</a>
        </li>
        <?php 
            if($paginas_totales>2){
                for ($i=2;$i<$paginas_totales;$i++){
                    $active="";
                    if ($i==$pagina){
                        $active="active";
                    }
                    echo "<li class='page-item ".$active."'>
                            <a class='page-link' href='?pagina=".$i."'>".$i."</a>
                        </li>";
                }
            }
        ?>
        <?php
            if($paginas_totales>1){
                $active="";
                if ($pagina==$paginas_totales){
                    $active="active";
                }
                echo "<li class='page-item ".$active."'>
                        <a class='page-link' href='?pagina=".$paginas_totales."'>".$paginas_totales."</a>
                    </li>";
            }
        ?>
        <li class="page-item <?php if($pagina==$paginas_totales){ echo "disabled"; } ?>">
            <a class="page-link" href="?pagina=<?php echo $pagina+1; ?>" disabled>Siguiente</a>
        </li>
    </ul>

    <?php
            if ($_REQUEST['noticia_eliminada']==1) {
            error();
            }
            function error(){

            echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                    Noticia eliminada correctamente
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    </div>";
            }
        ?>
</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>

<script>
    function redireccionarEditar(id_noticia){
        window.location.href="editarNoticia.php?id_noticia="+id_noticia;
    }
    function redireccionarEliminar(id_noticia){
        console.log(id_noticia);
        //window.location.href="eliminarNoticia.php?id_noticia="+id_noticia;
        document.forms['form'+id_noticia].submit();
    }
</script>