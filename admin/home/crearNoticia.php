<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $titulo = $_POST['titulo'];
    $cuerpo = $_POST['cuerpo'];
    $fecha = $_POST['fecha'];
    $fileName = $_FILES['img_dest']['name'];

    $instruccion = "insert into noticias(titulo,cuerpo,fecha,imagen) values ('$titulo', '".nl2br($cuerpo)."' ,'$fecha', null);";
    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");
    
    $id_noticia = mysqli_insert_id($conexion);

    ini_set('upload-max-filesize', '10M');
    ini_set('post_max_size', '10M');
    subirFoto($id_noticia);

    $instruccion = "update noticias set imagen='".$id_noticia.$fileName."' where id_noticia=".$id_noticia.";";
    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

    //header("location: /Proyectos/WebCooperativa/admin/home/crearNoticia.php?noticia_creada=1");
    echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/crearNoticia.php?noticia_creada=1') </script>";
    mysql_close($conexion);
}
?>
<style>
  #btn-crear-noticia{
      margin-top:1%;
      margin-bottom:3%;
      background-color:#125658;
      border:none;
  }
  #btn-crear-noticia:hover{
      background-color:#1E9599;
  }
  .form-width{
    padding-bottom:1%;
    width:100%;
    margin:auto;
  }
  @media (max-width: 1000px){
    .form-width{
      width:100%;
    }
    .container-fluid{
      width:100%;
      padding:1px;
      margin:1px;
    }
    .container{
      padding:0;
      margin:0;
    }
    .form-width{
      width:100%;
    }
  }
</style>

<!-- Begin Page Content -->
<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Noticias: crear noticia</h1>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="container">
            <form method="POST" enctype="multipart/form-data" style="text-align:left;padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
                <div class="form-group form-width">
                    <label>Titulo</label>
                    <input type="text" name="titulo" class="form-control input-form" placeholder="Ingrese el titulo" required>
                </div>
                <div class="form-group form-width">
                    <label>Fecha</label>
                    <input type="date" name="fecha" class="form-control"  required>
                </div>
 
                <label>Imagen</label>
                <div class="form-group form-width">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="img_dest" onchange="checkextension()"  required accept=".jpg,.jpeg,.png">
                    <label class="custom-file-label" for="inputGroupFile01">Seleccionar imagen</label>
                  </div>
                </div>

                <div class="form-group form-width">
                    <label for="exampleFormControlTextarea1">Cuerpo de la noticia</label>
                    <textarea name="cuerpo" class="form-control" rows="3"></textarea>
                </div>

                <input type="submit" value="Crear noticia" id="btn-crear-noticia" name="enviarNoticia" class="btn btn-primary">
                <?php
                if ($_REQUEST['noticia_creada']==1) {
                    error();
                }
                function error()
                {
                    echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                            Noticia creada correctamente
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                          </div>";
                }
                ?>
                <div id="div-alert" class="alert alert-warning form-width" role="alert" style="margin-top:3%;display:none;">
                  *Extension de archivo invalida. Debe seleccionar una imagen .jpg, .jpeg o .png
                </div>
            </form>
        </div>  
    </div>
</div>
<!-- /.container-fluid -->


<?php
function subirFoto($id_noticia)
{
    $message = '';
    if (isset($_POST['enviarNoticia']) && $_POST['enviarNoticia'] == 'Crear noticia') {
        // get details of the uploaded file
        $fileTmpPath = $_FILES['img_dest']['tmp_name'];
        $fileName = $_FILES['img_dest']['name'];
        $fileNameCmps = explode(".", $fileName);
        $fileExtension = strtolower(end($fileNameCmps));
        // check if file has one of the following extensions
        $allowedfileExtensions = array('jpg', 'png','jpeg');
        if (in_array($fileExtension, $allowedfileExtensions)) {
            // directory in which the uploaded file will be moved
            $uploadFileDir = '../../images/noticias/';
            $dest_path = $uploadFileDir . $id_noticia . $fileName;
            if (move_uploaded_file($fileTmpPath, $dest_path)) {
                $message ='Imagen subida correctamente.';
            } else {
                $message = 'Hubo un problema subiendo la imagen al servidor.';
            }
        } else {
            $message = 'Subida fallida. Tipos de imagen permitidos: ' . implode(',', $allowedfileExtensions);
        }
    }
}
?>

<?php
    include_once('panelFin.php');
?>
<script type="text/javascript">
    $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
    });
</script>

<script type="text/javascript">
    function checkextension() {
        var file = document.querySelector("#inputGroupFile01");
        if ( /\.(jpg|png|jpeg)$/i.test(file.files[0].name) === false ) {
            document.getElementById("inputGroupFile01").value="";
            document.getElementById("div-alert").style.display = "block";
        }
    }
</script>