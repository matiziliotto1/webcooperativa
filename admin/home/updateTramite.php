<?php
    include("../configDB.php");
    include_once('chequear_admin.php');
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id_tramite = $_POST['id_tramite'];
    $new_titulo = $_POST['titulo'];
    $new_descripcion = $_POST['descripcion'];
    $new_pdf = $_FILES['pdf']['name'];
    $instruccion = "";
    if ($new_pdf) {
        ini_set('upload-max-filesize', '10M');
        ini_set('post_max_size', '10M');
        subirPDF();
        $instruccion = "update tramites_formularios set titulo='".$new_titulo."',descripcion='".nl2br($new_descripcion)."',pdf='".$new_pdf."' where id_tramite=".$id_tramite.";";
    } else {
        $instruccion = "update tramites_formularios set titulo='".$new_titulo."',descripcion='".nl2br($new_descripcion)."' where id_tramite=".$id_tramite.";";
    }
    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

    echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/editarTramite.php?id_tramite=".$id_tramite."&tramite_editado=1') </script>";
    mysqli_close($conexion);
}
?>

<?php
function subirPDF()
{
    $message = '';
    // get details of the uploaded file
    $fileTmpPath = $_FILES['pdf']['tmp_name'];
    $fileName = $_FILES['pdf']['name'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    // check if file has one of the following extensions
    $allowedfileExtensions = array('pdf');
    if (in_array($fileExtension, $allowedfileExtensions)) {
        // directory in which the uploaded file will be moved
        $uploadFileDir = '../../pdf/';
        $dest_path = $uploadFileDir . $fileName;
        if (move_uploaded_file($fileTmpPath, $dest_path)) {
            $message ='Archivo subido correctamente.';
        } else {
            $message = 'Hubo un problema subiendo el archivo al servidor.';
        }
    } else {
        $message = 'Subida fallida. Tipos de archivo permitidos: ' . implode(',', $allowedfileExtensions);
    }
}
?>
