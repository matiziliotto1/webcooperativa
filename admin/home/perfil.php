<?php
	session_start();
?>
<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
	if($_SERVER["REQUEST_METHOD"] == "POST") {

		$nombre=$_POST['nombre'];
		$apellido=$_POST['apellido'];

		$sql = "update usuarios set nombre='$nombre', apellido='$apellido' where id_usuario=".$_SESSION['id_usuario'].";";

		$result = mysqli_query($conexion,$sql) or die ("Fallo en la consulta");
		$_SESSION['usuario'] = $nombre." ".$apellido;
		echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/perfil.php?perfil_editado=1') </script>";
		//header("location:  /Proyectos/webcooperativa/admin/home/perfil.php?perfil_editado=1");
	}
	
	$instruccion = "select * from usuarios where id_usuario=".$_SESSION['id_usuario'].";";

	$resultado = mysqli_query($conexion,$instruccion) or die ("Fallo en la consulta");

	$resultado = mysqli_fetch_array ($resultado);

	$nombre_aux = $resultado['nombre'];
	$apellido_aux = $resultado['apellido'];
	$email_aux = $resultado['email'];

?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-0 text-gray-800">Energia</h1>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div class="container">
      <form method="POST" action="perfil.php" enctype="multipart/form-data" style="text-align:left;padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
          
	  		<div class="form-group">
				<label for="email">E-mail</label>
				<input id="email" type="text" class="form-control" name="email" placeholder="Ingrese el e-mail" readonly="readonly" required
					<?php 
						if($email_aux){ 
							echo "value='$email_aux'";
						}
					?>
				>
			</div>
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input id="nombre" type="text" class="form-control" name="nombre" placeholder="Ingrese el nombre" required autofocus
					<?php 
						if($nombre_aux){ 
							echo "value='$nombre_aux'";
						}
					?>
				>
			</div>
			<div class="form-group">
				<label for="apellido">Apellido</label>
				<input id="apellido" type="text" class="form-control" name="apellido" placeholder="Ingrese el apellido" required 
					<?php 
						if($apellido_aux){ 
							echo "value='$apellido_aux'";
						}
					?>
				>
			</div>
			<div class="form-group" style="text-align: center;">
				<a class="btn btn-outline-primary" href="editarPassword.php">Cambiar contraseña</a>
			</div>
			<button type="submit" class="btn btn-primary">Guardar</button>
      </form>
	  <?php
        if ($_REQUEST['perfil_editado']==1) {
            error();
        }
        function error()
        {
            echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                    Perfil editado correctamente
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </div>";
		}
		mysqli_close($conexion);
    ?>
    </div>  
  </div>

</div>
<!-- /.container-fluid -->

<?php
   include_once('panelFin.php');
?>