<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<style>
    @media (max-width:1000px){
        h2{
            font-size: 1.2rem;
        }
        h4{
            font-size: 1rem;
        }
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-0 text-gray-800">Panel de control</h1>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="container">
            <h2 style="color:black;margin-top:5%;margin-bottom:3%;text-align: center;">Bienvenido al panel de control <?php echo $_SESSION['usuario'] ?></h2>

            <h4 style="padding-left:5%;">En el sector izquierdo encontrara todas las opciones para poder editar <br> 
            las secciones de la pagina web. <br>
            Tambien encontrara las opciones para crear, listar, editar y eliminar <br>
            las noticias y/o tramites y formularios que tengan actualmente.
        </h4>
        </div>

  </div>
</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>