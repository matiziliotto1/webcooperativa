<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
    $instruccion = "select * from noticias where id_noticia=".$_REQUEST['id_noticia'].";";

    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

    $fila = mysqli_fetch_array($consulta);
    $titulo = $fila['titulo'];
    $cuerpo = $fila['cuerpo'];
    $fecha = $fila['fecha'];
    $imagen = $fila['imagen'];

    $id_noticia = $_REQUEST['id_noticia'];

    mysqli_close($conexion);
?> 


<!-- Begin Page Content -->
<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Noticias: editar noticia</h1>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="container">
            <form method="POST" action="updateNoticia.php" enctype="multipart/form-data" style="padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
                <input type="number" name="id_noticia" value="<?php echo $id_noticia ?>" hidden>
                <div class="form-group">
                    <label for="tituloInput">Titulo</label>
                    <input class="form-control" type="text" name="titulo" value="<?php echo $titulo; ?>" id="tituloInput" required>
                </div>
                <div class="form-group">
                    <label for="fechaInput">Fecha</label>
                    <input class="form-control" type="date" name="fecha" value="<?php echo $fecha; ?>" required>
                </div>
                <?php
                if ($imagen) {
                    echo "
                    <div class='form-group'>
                        <label>Imagen actual</label>
                        <br>
                        <img id='imgSalida' class='img-fluid' src='../../images/noticias/$imagen' width='400' height='400'>
                    </div>";
                }
                    
                ?>
                
                <label>Imagen</label>
                <div class="form-group form-width">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="img_dest" accept=".jpg,.png,.jpeg">
                    <label class="custom-file-label" for="inputGroupFile01">Seleccionar imagen</label>
                  </div>
                </div>
                <div id="div-alert" class="alert alert-warning form-width" role="alert" style="margin-top:3%;display:none;">
                  *Extension de archivo invalida. Debe seleccionar una imagen .jpg, .jpeg o .png
                </div>

                <div class="form-group">
                    <label for="cuerpoInput">Cuerpo</label>
                    <textarea class="form-control h-50" rows="5" name="cuerpo" id="cuerpoInput" required><?php echo $cuerpo; ?></textarea>
                </div>
                
                <input type="submit" class="btn btn-primary" value="Editar Noticia">

                
            </form>
            <?php
            if ($_REQUEST['noticia_editada']==1) {
                error();
            }
            function error()
            {
                echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                        Noticia editada correctamente
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        </div>";
            }
            ?>
        </div>  
    </div>
</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>

<script type="text/javascript">
    $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
    });
    $('#inputGroupFile01').change(function(e) {
            var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
            if ( !allowedExtensions.exec($('#inputGroupFile01').val())) {
                $('#inputGroupFile01').val('');
                document.getElementById("div-alert").style.display = "block";
            } else {
                addImage(e); 
            }
            });

            function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
            
            if (!file.type.match(imageType))
            return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            }
        
            function fileOnload(e) {
            var result=e.target.result;
            $('#imgSalida').attr("src",result);
            }
</script>