<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
    $instruccion = "select * from tramites_formularios where id_tramite=".$_REQUEST['id_tramite'].";";

    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar Tramite o Formulario");

    $fila = mysqli_fetch_array($consulta);
    $id_tramite=$_REQUEST['id_tramite'];
    $titulo = $fila['titulo'];
    $descripcion = $fila['descripcion'];
    $pdf = $fila['pdf'];

    mysqli_close($conexion);
?> 


<!-- Begin Page Content -->
<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Editar Tramites y Formularios</h1>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="container">
            <form method="POST" action="updateTramite.php" enctype="multipart/form-data" style="padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
                <input type="number" name="id_tramite" value="<?php echo $id_tramite ?>" hidden>    
                <div class="form-group">
                    <label for="tituloInput">Titulo</label>
                    <input class="form-control" type="text" name="titulo" value="<?php echo $titulo; ?>" id="tituloInput" required>
                </div>
                <div class="form-group">
                    <label for="fechaInput">Descripcion</label>
                    <textarea class="form-control" rows="5" name="descripcion" id="descripcionInput" required><?php echo $descripcion; ?></textarea>
                </div>
                <label>PDF</label>
                <div class="form-group form-width">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="pdf" onchange="checkextension()" accept=".pdf">
                    <label class="custom-file-label" for="inputGroupFile01"><?php echo $pdf ?></label>
                  </div>
                </div>
                
                <input type="submit" class="btn btn-primary" value="Editar tramite">

                
            </form>
            <?php
            if ($_REQUEST['tramite_editado']==1) {
                error();
            }
            function error()
            {
                echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                        Tramite editado correctamente
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        </div>";
            }
            ?>
            <div id="div-alert" class="alert alert-warning form-width" role="alert" style="margin-top:3%;display:none;">
                *Extension de archivo invalida. Debe seleccionar un archivo .pdf
            </div>
        </div>  
    </div>
</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>

<script type="text/javascript">
    $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
    });
</script>

<script type="text/javascript">
    function checkextension() {
        var file = document.querySelector("#inputGroupFile01");
        if ( /\.(pdf)$/i.test(file.files[0].name) === false ) {
            document.getElementById("inputGroupFile01").value="";
            document.getElementById("div-alert").style.display = "block";
        }
    }
</script>