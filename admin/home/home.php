<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $primer_subtexto_aux = $_POST['primer_subtexto'];
    if ($_POST['primer_subtexto_mostrar']=='on') {
        $primer_subtexto_aux_mostrar = 1;
    } else {
        $primer_subtexto_aux_mostrar = 0;
    }
    $subtexto_tramites_formularios_aux = $_POST['subtexto_tramites_formularios'];
    if ($_POST['subtexto_tramites_formularios_mostrar']=='on') {
        $subtexto_tramites_formularios_aux_mostrar = 1;
    } else {
        $subtexto_tramites_formularios_aux_mostrar = 0;
    }
    $titulo_imagen_aux = $_POST['titulo_imagen'];
    if ($_POST['titulo_imagen_mostrar']=='on') {
        $titulo_imagen_aux_mostrar = 1;
    } else {
        $titulo_imagen_aux_mostrar = 0;
    }
    $seccion_cooperativa_titulo_aux = $_POST['seccion_cooperativa_titulo'];
    if ($_POST['seccion_cooperativa_titulo_mostrar']=='on') {
        $seccion_cooperativa_titulo_aux_mostrar = 1;
    } else {
        $seccion_cooperativa_titulo_aux_mostrar = 0;
    }
    $seccion_cooperativa_subtexto_aux = $_POST['seccion_cooperativa_subtexto'];
    if ($_POST['seccion_cooperativa_subtexto_mostrar']=='on') {
        $seccion_cooperativa_subtexto_aux_mostrar = 1;
    } else {
        $seccion_cooperativa_subtexto_aux_mostrar = 0;
    }
    
    $instruccion1 = "update home set texto='".nl2br($primer_subtexto_aux)."', ver='".$primer_subtexto_aux_mostrar."' where descripcion='primer_subtexto'";
    $consulta1 = mysqli_query($conexion, $instruccion1) or die("Fallo en la consulta1");

    $instruccion2 = "update home set texto='".nl2br($subtexto_tramites_formularios_aux)."', ver='".$subtexto_tramites_formularios_aux_mostrar."' where descripcion='subtexto_tramites_formularios'";
    $consulta2 = mysqli_query($conexion, $instruccion2) or die("Fallo en la consulta2");

    $instruccion3 = "update home set texto='".nl2br($titulo_imagen_aux)."', ver='".$titulo_imagen_aux_mostrar."' where descripcion='titulo_imagen'";
    $consulta3 = mysqli_query($conexion, $instruccion3) or die("Fallo en la consulta3");

    $instruccion4 = "update home set texto='".nl2br($seccion_cooperativa_titulo_aux)."', ver='".$seccion_cooperativa_titulo_aux_mostrar."' where descripcion='seccion_cooperativa_titulo'";
    $consulta4 = mysqli_query($conexion, $instruccion4) or die("Fallo en la consulta4");

    $instruccion5 = "update home set texto='".nl2br($seccion_cooperativa_subtexto_aux)."', ver='".$seccion_cooperativa_subtexto_aux_mostrar."' where descripcion='seccion_cooperativa_subtexto'";
    $consulta5 = mysqli_query($conexion, $instruccion5) or die("Fallo en la consulta5");

    $instruccion6 = "update home set ver='".$seccion_cooperativa_imagen_aux_mostrar."' where descripcion='seccion_cooperativa_imagen'";
    $consulta6 = mysqli_query($conexion, $instruccion6) or die("Fallo en la consulta6");
    
    $instruccion4 = "update home set texto='".nl2br($seccion_cooperativa_titulo_aux)."', ver='".$seccion_cooperativa_titulo_aux_mostrar."' where descripcion='seccion_cooperativa_titulo'";
    $consulta4 = mysqli_query($conexion, $instruccion4) or die("Fallo en la consulta2");

    $instruccion5 = "update home set texto='".nl2br($seccion_cooperativa_subtexto_aux)."', ver='".$seccion_cooperativa_subtexto_aux_mostrar."' where descripcion='seccion_cooperativa_subtexto'";
    $consulta5 = mysqli_query($conexion, $instruccion5) or die("Fallo en la consulta3");
    //header("location: /Proyectos/WebCooperativa/admin/home/home.php?home_editado=1");
    echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/home.php?home_editado=1') </script>";
}
?>

<?php
$instruccion = "select * from home;";
$consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

$nfilas = mysqli_num_rows($consulta);

if ($nfilas > 0) {
    for ($i=0; $i<$nfilas; $i++) {
        $fila = mysqli_fetch_array($consulta);
        
        switch ($fila['descripcion']) {
            case 'primer_subtexto':
                $primer_subtexto = new stdClass();
                $primer_subtexto->texto = $fila['texto'];
                $primer_subtexto->mostrar = $fila['ver'];
                break;
            case 'subtexto_tramites_formularios':
                $subtexto_tramites_formularios = new stdClass();
                $subtexto_tramites_formularios->texto = $fila['texto'];
                $subtexto_tramites_formularios->mostrar = $fila['ver'];
                break;
            case 'titulo_imagen':
                $titulo_imagen = new stdClass();
                $titulo_imagen->texto = $fila['texto'];
                $titulo_imagen->mostrar = $fila['ver'];
                break;
            case 'seccion_cooperativa_titulo':
                $seccion_cooperativa_titulo = new stdClass();
                $seccion_cooperativa_titulo->texto = $fila['texto'];
                $seccion_cooperativa_titulo->mostrar = $fila['ver'];
                break;
            case 'seccion_cooperativa_subtexto':
                $seccion_cooperativa_subtexto = new stdClass();
                $seccion_cooperativa_subtexto->texto = $fila['texto'];
                $seccion_cooperativa_subtexto->mostrar = $fila['ver'];
                break;
            default:
                break;
        }
    }
}
mysqli_close($conexion);
?>

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-0 text-gray-800">Pagina de Inicio</h1>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div class="container">
      <form method="POST" enctype="multipart/form-data" style="text-align:left;padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
          <div class="form-group form-width">
            <label>Titulo de la imagen</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="titulo_imagen_mostrar" <?php if ($titulo_imagen->mostrar) {echo 'checked';}?>>
                </div>
              </div>
                <input type="text" name="titulo_imagen" class="form-control input-form" placeholder="Ingrese el titulo de la imagen" required value="<?php echo strip_tags($titulo_imagen->texto); ?>">
            </div>
          </div>
          <div class="form-group form-width">
            <label>Texto debajo de la imagen principal</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="primer_subtexto_mostrar" <?php if ($primer_subtexto->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <textarea name="primer_subtexto" class="form-control" rows="5" placeholder="Ingrese el titulo de la seccion"><?php echo $primer_subtexto->texto; ?></textarea>
            </div>
          </div>
          <div class="form-group form-width">
            <label>Texto de tramites y formularios</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="subtexto_tramites_formularios_mostrar" <?php if ($subtexto_tramites_formularios->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <textarea name="subtexto_tramites_formularios" class="form-control" rows="5" placeholder="Ingrese el texto debajo del titulo..."><?php echo strip_tags($subtexto_tramites_formularios->texto); ?></textarea>
            </div>
          </div>
          <div class="form-group form-width">
            <label>Titulo debajo de tramites y formularios</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="seccion_cooperativa_titulo_mostrar" <?php if ($seccion_cooperativa_titulo->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <input type="text" name="seccion_cooperativa_titulo" class="form-control" placeholder="Ingrese el titulo debajo de tramites y formularios..." value="<?php echo strip_tags($seccion_cooperativa_titulo->texto); ?>">
            </div>
          </div>
          <div class="form-group form-width">
            <label>Texto debajo de tramites y formularios</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="seccion_cooperativa_subtexto_mostrar" <?php if ($seccion_cooperativa_subtexto->mostrar) {echo 'checked';}?>>
                </div>
              </div>
              <textarea name="seccion_cooperativa_subtexto" class="form-control" rows="5" placeholder="Ingrese el texto debajo del titulo de tramites y formularios..."><?php echo strip_tags($seccion_cooperativa_subtexto->texto); ?></textarea>
            </div>
          </div>

          <input type="submit" value="Actualizar" class="btn btn-primary">
          
      </form>
      <?php
        if ($_REQUEST['home_editado']==1) {
            error();
        }
        function error()
        {
            echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                    Pagina principal editada correctamente
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </div>";
        }
        ?>
    </div>  
  </div>

</div>
<!-- /.container-fluid -->

<?php
    include_once('panelFin.php');
?>