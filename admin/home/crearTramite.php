<?php
    include_once('panelInicio.php');
    include_once('chequear_admin.php');
?>
<?php
    include("../configDB.php");
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $titulo = $_POST['titulo'];
    $descripcion = $_POST['descripcion'];
    $fileName = $_FILES['pdf']['name'];

    $instruccion = "insert into tramites_formularios(titulo,descripcion,pdf) values ('$titulo', '".nl2br($descripcion)."' ,'$fileName');";

    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");

    ini_set('upload-max-filesize', '10M');
    ini_set('post_max_size', '10M');
    subirPDF();
    
    //header("location: /Proyectos/WebCooperativa/admin/home/crearTramite.php?Tramites_Formularios_creado=1");
    echo "<script> window.location.replace('/Proyectos/webcooperativa/admin/home/crearTramite.php?Tramites_Formularios_creado=1') </script>";
    mysql_close($conexion);
}
?>
<style>
  #btn-crear-tramite_formulario{
      margin-top:1%;
      margin-bottom:3%;
      background-color:#125658;
      border:none;
  }
  #btn-crear-tramite_formulario:hover{
      background-color:#1E9599;
  }
  .form-width{
    padding-bottom:1%;
    width:100%;
    margin:auto;
  }
  @media (max-width: 1000px){
    .form-width{
      width:100%;
    }
    .container-fluid{
      width:100%;
      padding:1px;
      margin:1px;
    }
    .container{
      padding:0;
      margin:0;
    }
    .form-width{
      width:100%;
    }
  }
</style>

<!-- Begin Page Content -->
<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Crear Tramites y/o Formularios</h1>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="container">
            <form method="POST" enctype="multipart/form-data" style="text-align:left;padding-top: 1%;padding-bottom: 1%;margin-block-end:-0em;" id="form">
                <div class="form-group form-width">
                    <label>Titulo</label>
                    <input type="text" name="titulo" class="form-control input-form" placeholder="Ingrese el titulo" required>
                </div>
                <div class="form-group form-width">
                    <label>Descripcion</label>
                    <textarea name="descripcion" rows="5" class="form-control input-form" placeholder="Ingrese la descripcion..."></textarea>
                </div>
 
                <label>PDF</label>
                <div class="form-group form-width">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="pdf" onchange="checkextension()"  required accept=".pdf">
                    <label class="custom-file-label" for="inputGroupFile01">Seleccionar archivo PDF</label>
                  </div>
                </div>

                <input type="submit" value="Crear" id="btn-crear-tramite_formulario" name="crearT_F" class="btn btn-primary">
                <?php
                if ($_REQUEST['Tramites_Formularios_creado']==1) {
                    error();
                }
                function error()
                {
                    echo "<div class='alert alert-success alert-dismissible fade show mx-auto form-width' role='alert'>
                            Tramite creado correctamente
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                          </div>";
                }
                ?>
                <div id="div-alert" class="alert alert-warning form-width" role="alert" style="margin-top:3%;display:none;">
                  *Extension de archivo invalida. Debe seleccionar un archivo .pdf
                </div>
            </form>
        </div>  
    </div>
</div>
<!-- /.container-fluid -->


<?php
function subirPDF()
{
    $message = '';
    // get details of the uploaded file
    $fileTmpPath = $_FILES['pdf']['tmp_name'];
    $fileName = $_FILES['pdf']['name'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));
    // check if file has one of the following extensions
    $allowedfileExtensions = array('pdf');
    if (in_array($fileExtension, $allowedfileExtensions)) {
        // directory in which the uploaded file will be moved
        $uploadFileDir = '../../pdf/';
        $dest_path = $uploadFileDir . $fileName;
        if (move_uploaded_file($fileTmpPath, $dest_path)) {
            $message ='Archivo subido correctamente.';
        } else {
            $message = 'Hubo un problema subiendo el archivo al servidor.';
        }
    } else {
        $message = 'Subida fallida. Tipos de archivo permitidos: ' . implode(',', $allowedfileExtensions);
    }
}
?>

<?php
    include_once('panelFin.php');
?>
<script type="text/javascript">
    $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
    });
</script>

<script type="text/javascript">
    function checkextension() {
        var file = document.querySelector("#inputGroupFile01");
        if ( /\.(pdf)$/i.test(file.files[0].name) === false ) {
            document.getElementById("inputGroupFile01").value=""; 
            document.getElementById("div-alert").style.display = "block";
        }
    }
</script>