<?php
    include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; Resultados</title>
<?php
    $buscar = $_GET['buscar'];

    $instruccion1 = "select * from noticias where upper(titulo) like upper('%".$buscar."%');";
    $consulta1 = mysqli_query($conexion, $instruccion1) or die("Fallo en la consulta");
    $nfilas1 = mysqli_num_rows($consulta1);

    $instruccion2 = "select * from tramites_formularios where upper(titulo) like upper('%".$buscar."%');";
    $consulta2 = mysqli_query($conexion, $instruccion2) or die("Fallo en la consulta");
    $nfilas2 = mysqli_num_rows($consulta2);
?>
<style>
    @media (min-width: 768px){
        .site-section{
        padding: 8em 0;
        }
    }
    @media (max-width:1000px){
        .site-section{
            padding: 2.5em;
        }
        .thumbnail.order-md-2{
            height: 0;
            width: 0;
        }
    }
</style>
<div class="site-section" style="background-color: #F2F2F2;">
    <div class="container bg-white p-5">
        <h2 class="color-titulos">
            Resultados
        </h2>
        <?php
            if ($nfilas1 > 0) {
                echo "<h5>Noticias encontradas</h5>";
                for ($i=0; $i<$nfilas1; $i++) {
                    $fila = mysqli_fetch_array($consulta1);

                    setlocale(LC_TIME, 'es_ES', 'esp_esp'); 
                    $año = substr($fila['fecha'],0,4); 
                    $mes = substr($fila['fecha'],5,2); 
                    $dia = substr($fila['fecha'],8,2);
                    
                    $mes = ucfirst(strftime("%B", mktime(0, 0, 0, $mes)));

                    $fecha = $dia." de ".$mes." ".$año;

                    print(
                        '<div class="post-entry-2 d-flex p-3" style="-webkit-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                        -moz-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                        box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);">
                        <div class="thumbnail order-md-2" style="background-image: url('."'images/".$fila['imagen']."'".')"></div>
                        <div class="contents order-md-1 pl-0">
                            <h4><a href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'" style="color: #2c2c2c;">'.$fila['titulo'].'</a></h4>
                            <p class="mb-3">'.substr($fila['cuerpo'], 0, 250).'...</p>
                            <div class="post-meta">
                            <span class="d-block"><a class="color-links" href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'">Ver noticia</a></span>
                            <span class="date-read">'.$fecha.'<span class="mx-1">
                            </div>
                        </div>
                        </div>'
                    );
                }
                
            }
            if ($nfilas2 > 0) {
                echo "<h5>Tramites y formularios encontrados</h5>";
                for ($i=0; $i<$nfilas2; $i++) {
                    $fila = mysqli_fetch_array($consulta2);

                    print(
                        '<div class="d-flex p-3" style="-webkit-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                        -moz-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                        box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);margin-bottom:30px;">
                        <div class="contents order-md-1 pl-0">
                            <h4 style="color: #2c2c2c;">'.$fila['titulo'].'</h4>
                            <p class="mb-3">'.$fila['descripcion'].'</p>
                            <a class="color-links" href="descargar_tramite.php?id_tramite='.$fila['id_tramite'].'">Descargar formulario</a>
                        </div>
                        </div>'
                    );
                }
                
            }
            if($nfilas1==0 && $nfilas2==0){
                echo "<h4>No se encontraron noticias o tramites y formularios para el texto ingresado</h4>";
            }
            mysqli_close($conexion);
        ?>

    </div>
</div>

<?php
    include_once('footer.php');
?>