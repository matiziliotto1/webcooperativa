<?php
  include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; Tramites y formularios</title>
<div class="site-section py-0 m-0">
    <div class="owl-carousel hero-slide">
        <div class="site-section">
            <div class="">
                <div class="half-post-entry d-block d-lg-flex bg-light" style="height: 35%">
                    <div style="position: relative;display: inline-block;">
                        <img src="images/tramites_formularios.jpg" width="100%">
                        <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 30%;top: 50%;width: 60%;">
                            <h1>Tramites y Formularios</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-section" style="background-color: #F2F2F2;">
    <div class="container bg-white p-5">
        <h2 class="color-titulos">
            Tramites y formularios
        </h2>
        <?php
        $instruccion = "select * from tramites_formularios ;";

        $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar noticias");

        $nfilas = mysqli_num_rows($consulta);
        if ($nfilas > 0) {
            for ($i=0; $i<$nfilas; $i++) {
                $fila = mysqli_fetch_array($consulta);

                print(
                    '<div class="d-flex p-3" style="-webkit-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                    -moz-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                    box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);margin-bottom:30px;">
                    <div class="contents order-md-1 pl-0">
                        <h4 style="color: #2c2c2c;">'.$fila['titulo'].'</h4>
                        <p class="mb-3">'.$fila['descripcion'].'</p>
                        <a class="color-links" href="descargar_tramite.php?id_tramite='.$fila['id_tramite'].'">Descargar formulario</a>
                    </div>
                    </div>'
                );
            }
        }
        mysqli_close($conexion);
        ?>
    </div>
</div>
<?php
    include_once('footer.php');
?>