<?php
    include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; Inicio</title>
    <div class="site-section py-0 m-0">
      <div class="owl-carousel hero-slide">

      <div class="site-section">
          <div class="">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div style="position: relative;display: inline-block;">
                <img src="images/foto_Ceez_1.png">
                <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 0;top: 50%;text-align: center;width: 60%;">
                    <h1>
                      <?php
                        $instruccion = "select * FROM `home` WHERE Descripcion='titulo_imagen';";
                        $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar titulo_imagen");
                        $fila = mysqli_fetch_array($consulta);
                        if ($fila['ver']==1) {
                            echo ($fila['texto']);
                        }
                        ?>
                    </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="site-section">
          <div class="">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div style="position: relative;display: inline-block;">
                <img src="images/foto_Ceez_2.png">
                <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 0;top: 50%;text-align: center;width: 60%;">
                    <h1>
                      <?php
                        $instruccion = "select * FROM `home` WHERE Descripcion='titulo_imagen';";
                        $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar titulo_imagen");
                        $fila = mysqli_fetch_array($consulta);
                        if ($fila['ver']==1) {
                            echo ($fila['texto']);
                        }
                        ?>
                    </h1>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="site-section">
          <div class="">
            <div class="half-post-entry d-block d-lg-flex bg-light">
              <div style="position: relative;display: inline-block;">
                <img src="images/foto_Ceez_3.png">
                <div class="titulo-img" style="color:white;position: absolute;z-index: 999;margin: 0 auto;left: 0;right: 0;top: 50%;text-align: center;width: 60%;">
                    <h1>
                      <?php
                        $instruccion = "select * FROM `home` WHERE Descripcion='titulo_imagen';";
                        $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar titulo_imagen");
                        $fila = mysqli_fetch_array($consulta);
                        if ($fila['ver']==1) {
                            echo ($fila['texto']);
                        }
                        ?>
                    </h1>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div>	
      <div class="container">
        <div class="row" style="text-align: center;">
          <p>
            <?php
            $instruccion = "select * FROM `home` WHERE Descripcion='primer_subtexto';";
            $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar primer_subtexto");
            $fila = mysqli_fetch_array($consulta);
            if ($fila['ver']==1) {
                echo ($fila['texto']);
            }
            ?>
          </p>
          <hr style="background-color: #cecece!important;height: 0.05px!important;width: 80%!important;">
        </div>
      </div>
    </div>
  

    <div class="site-section">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">

            <div class="col-md-3" style="background-color: #125658;">
              <div class="post-entry-1">
                <div class="contents">
                  <h1 class="titulo_novedades">Novedades</h1>

                  <div class="tabla_novedades">
                      <hr>
                      <h4><a href="noticias.php" style="color:#dfdfdf">Ver noticias &rsaquo;</a></h4>
                      <hr>
                      <h4><a href="sepelios.php" style="color:#dfdfdf">Sepelios &rsaquo;</a></h4>
                      <hr>
                      <h4><a href="gnc.php" style="color:#dfdfdf">GNC &rsaquo;</a></h4>
                      <hr>
                      <h4><a href="farmacias.php" style="color:#dfdfdf">Farmacias &rsaquo;</a></h4>
                      <hr>
                  </div>                    
                </div>
              </div>
            </div>

            <div class="col-md-9 bg-light" style="-webkit-box-shadow: inset 0px 0px 40px -30px rgba(0,0,0,0.75);-moz-box-shadow: inset 0px 0px 40px -30px rgba(0,0,0,0.75);box-shadow: inset 0px 0px 40px -30px rgba(0,0,0,0.75);">
              <h2 style="text-align:center;" class="color-titulos">Ultimas noticias</h2>

              <?php
                $instruccion = "select * from noticias order by fecha desc LIMIT 0,3;";

                $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar noticias");
    
                $nfilas = mysqli_num_rows($consulta);
                if ($nfilas > 0) {
                    for ($i=0; $i<$nfilas; $i++) {
                        $fila = mysqli_fetch_array($consulta);

                        setlocale(LC_TIME, 'es_ES', 'esp_esp');
                        $año = substr($fila['fecha'], 0, 4);
                        $mes = substr($fila['fecha'], 5, 2);
                        $dia = substr($fila['fecha'], 8, 2);
                        
                        $mes = ucfirst(strftime("%B", mktime(0, 0, 0, $mes)));

                        $fecha = $dia." de ".$mes." ".$año;

                        echo '<div class="post-entry-2 d-flex" style="padding-left: 15%;padding-top: 0%;">
                                <div class="thumbnail" style="margin-right: 1%;background-image:url(images/noticias/'.$fila['imagen'].');"></div>
                                <div class="contents">
                                  <h2><a href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'">'.$fila['titulo'].'</a></h2>
                                  <div class="post-meta">
                                    <span class="date-read"><span class="mx-1">&bullet;</span>'.$fecha.'<span class="mx-1">&bullet;</span></span>
                                  </div>
                                  <p class="mb-3">'.substr($fila['cuerpo'], 0, 200).'...</p><a class="color-links" href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'">Ver noticia completa</a>
                                </div>
                              </div>';
                    }
                }
                ?>     
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END section -->
    <hr style="background-color: #cecece!important;height: 0.05px!important;width: 80%!important;">

    <div class="site-section">
      <div class="">
        <div class="half-post-entry d-block d-lg-flex bg-light" style="min-height: 20rem;">
          <div class="col-6" style="background-image: url('images/tramites_y_formularios.jpeg')"></div>
          <div style="-webkit-box-shadow: inset 0px 0px 40px -30px rgba(0,0,0,0.75);
              -moz-box-shadow: inset 0px 0px 40px -30px rgba(0,0,0,0.75);
              box-shadow: inset 0px 0px 40px -30px rgba(0,0,0,0.75);">
            <div style="padding: 5%;">
              <h2 style="color: #2A2A2A">Tramites y formularios</h2>
              <hr>
                <p>
                    <?php
                    $instruccion = "select * FROM `home` WHERE Descripcion='subtexto_tramites_formularios';";
                    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");
                    $fila = mysqli_fetch_array($consulta);
                    if ($fila['ver']==1) {
                        echo ($fila['texto']);
                    }
                    ?>
                </p>
                <a href="tramites_formularios.php" class="color-links">Tramites y formularios &rsaquo;</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <hr style="background-color: #cecece!important;height: 0.05px!important;width: 80%!important;">

    <div class="site-section">
      <div class="row">
        <div class="col-lg-12">
          <div class="row" style="background-image: url('images/historia_cooperativa.png');background-repeat: no-repeat;background-position: center center;background-size: cover;background-attachment: fixed;">
            <div class="col-12">
              <h1 style="color: white; text-align: center">
                <?php
                  $instruccion = "select * FROM `home` WHERE Descripcion='seccion_cooperativa_titulo';";
                  $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");
                  $fila = mysqli_fetch_array($consulta);
                  if ($fila['ver']==1) {
                      echo ($fila['texto']);
                  }
                  ?>
              </h1>
              <p class="mb-3" style="color: white; text-align: center">
                <?php
                  $instruccion = "select * FROM `home` WHERE Descripcion='seccion_cooperativa_subtexto';";
                  $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la consulta");
                  $fila = mysqli_fetch_array($consulta);
                  if ($fila['ver']==1) {
                      echo ($fila['texto']);
                  }
                  ?>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php
    include_once('footer.php');
?>