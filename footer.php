        <div class="site-section bg-light">
                <div class="row">
                    <div class="col-5">
                        <div class="col-12 col-lg-12 d-flex mx-auto">
                            <a href="home.php" class="site-logo">
                                <img class="img-fluid" src="images/logo.jpg" width="150" height="100">
                            </a>

                            <div style="padding-left:3%;">
                                <h5 style="color:#303030;">
                                    Cooperativa energia electrica Zapala
                                </h5>
                                <h6>
                                    Luis Monti 434
                                    <br>
                                    CP: 8340 - Zapala 
                                    <br>
                                    Teléfono: (+54) (02942) 421291
                                </h6>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-7" style="color:#303030;">
                        <div class="d-lg-inline-block pr-3">
                            <h5><a href="contacto.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Contacto</a></h5>
                            <h5><a href="sepelios.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Sepelios</a></h5>
                            <h5><a href="farmacia.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Farmacia</a></h5>
                            <h5><a href="tramites_formularios.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Tramites y formularios</a></h5>
                        </div>
                        <div class="d-lg-inline-block pr-3">
                            <h5><a href="energia.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Energia</a></h5>
                            <h5><a href="gnc.php" style="color:#303030;"><span class="mx-1">&bullet;</span>GNC</a></h5>
                            <h5><a href="noticias.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Noticias</a></h5>
                            <h5><a href="centro_comunicaciones.php" style="color:#303030;"><span class="mx-1">&bullet;</span>Centro de comunicaciones</a></h5>
                        </div>
                        
                        <div class="d-lg-inline-block top-social d-none" style="border-left: 1px solid #a4adb3;">
                            <a href="#" class="d-inline-block p-3 pl-4"><span class="icon-facebook"></span></a>
                            <br>
                            <a href="#" class="d-inline-block p-3 pl-4"><span class="icon-twitter"></span></a>
                            <br>
                            <a href="#" class="d-inline-block p-3 pl-4"><span class="icon-instagram"></span></a>
                        </div>

                    </div>

                </div>
        </div>
        
        <div class="footer" style="background-color: #0c4749;color: white;">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copyright">
                            <h6>
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="https://www.agenciamarcar.com" target="_blank" style="color:black;">MarcAR</a>
                            </h6>
                            <p style="display: none;">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .site-wrap -->
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#0F4E4E"/></svg></div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.mb.YTPlayer.min.js"></script>
    <script src="js/main.js"></script>

</body>