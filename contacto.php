<?php
    include_once('header.php');
?>
<?php
    include("admin/configDB.php");
?>
<title>Cooperativa &mdash; Contacto</title>
<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        require 'PHPMailer/PHPMailerAutoload.php';
        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        $mail->IsSMTP();
        
        //Configuracion servidor mail
        $mail->From = "turcotractores@gmail.com"; //remitente
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls'; //seguridad
        $mail->Host = "smtp.gmail.com"; // servidor smtp
        $mail->Port = 587; //puerto
        $mail->Username ='turcotractores@gmail.com'; //nombre usuario
        $mail->Password = 'turco123'; //contraseña
        
        $mail->AddAddress('turcotractores@gmail.com');
        $mail->Subject = 'Comentario en la web';
        $mail->Body = 'Mensaje enviado por: 
        Nombre: '.$_POST['nombre'].'
        Apellido: '.$_POST['apellido'].'
        Telefono: '.$_POST['telefono'].'
        Email: '.$_POST['email'].'

        '.$_POST['mensaje'];

        //===================================
        /* TODO: ASI QUEDO EN EL SERVIDOR
        require 'PHPMailer/PHPMailerAutoload.php';
        
        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        
        $mail->IsSMTP(); 
        $mail->SMTPAuth   = true; 
        $mail->Port       = 25; 
        $mail->Host       = "ceez.com.ar"; // SMTP server
        $mail->Username   = "contacto@ceez.com.ar";  
        $mail->Password   = "hc9usvCPNKXg";
    
        $mail->From       = "contacto@ceez.com.ar";
        $mail->FromName   = "C.E.E.Z";
        $mail->AddAddress("contacto@ceez.com.ar");
        $mail->addReplyTo($_POST['email']);
        
        $mail->Subject = 'Contacto: '.$_POST['nombre'].' '.$_POST['apellido'];
        $mail->Body = 'Mensaje enviado por: 
        Nombre: '.$_POST['nombre'].'
        Apellido: '.$_POST['apellido'].'
        Telefono: '.$_POST['telefono'].'
        Email: '.$_POST['email'].'
        Mensaje: '.$_POST['mensaje'];
        */
        //===================================

        //Avisar si fue enviado o no y dirigir al index
        if ($mail->Send()) {
            $mostrar=true;
            $titulo='Mensaje Enviado';
            $mensaje='Su mensaje fue enviado correctamente';
        } else {
            $mostrar=true;
            $titulo='Mensaje No Enviado';
            $mensaje='A ocurrido un error al enviar su mensaje, intente nuevamente mas tarde';
        }
    }
?>

<style>
    @media (min-width: 768px){
        .site-section{
            padding: 8em 0;
        }
    }
    @media (max-width:1000px){
        .site-section{
            padding: 2.5em;
        }
    }
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }

    input[type=number] { 
        -moz-appearance:textfield; 
    }
    .botonEnviar{
        border-radius: 5px;
        background-color: #0c4346;
        cursor: pointer;
        color: white;
        margin-top: 2%;
        border: none;
        width: auto;
        padding: 1%;
        padding-left: 3%;
        padding-right: 3%;
        -webkit-box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.45);
        -moz-box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.45);
        box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.45);
        justify-content: center;
    }
    .botonEnviar:hover{
        background-color: #44C5CA;
        color: black;
    }
    /* Tamaño del scroll */
    #mensaje::-webkit-scrollbar {
    width: 8px;
    height: 8px;
    cursor: pointer;
    }

    #mensaje{
        scrollbar-color: rgba(0, 0, 0, .5) rgba(0, 0, 0, 0);
        scrollbar-width: thin;
        cursor: pointer;
        resize: none; 
        background-color: #e9ecef; 
        border: 1px solid #ced4da;
        border-radius: 5px;
    }

    /* Estilos barra (thumb) de scroll */
    #mensaje::-webkit-scrollbar-thumb {
    background: #ccc;
    border-radius: 4px;
    }

    #mensaje::-webkit-scrollbar-thumb:active {
    background-color: #999999;
    }

    #mensaje::-webkit-scrollbar-thumb:hover {
    background: #b3b3b3;
    box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
    }

    /* Estilos track de scroll */
    #mensaje::-webkit-scrollbar-track {
    background: #e1e1e1;
    border-radius: 4px;
    }

    #mensaje::-webkit-scrollbar-track:hover, 
    #mensaje::-webkit-scrollbar-track:active {
    background: #d4d4d4;
    }

    .style-label{
        border-left: 6px solid #0c4346;
    }
</style>
<div class="site-section" style="background-color: #F2F2F2;">
    <div class="container bg-white p-5">
        <h2 class="color-titulos">
            Contacto
        </h2>

        <div class="col">
            <form method="POST" enctype="multipart/form-data" id="form">
                <div class="row mt-4 style-label">
                    <label style="color: black; font-weight: bold; margin-left: 3%;">Nombre *</label>
                </div>
                <div class="row mt-2">
                    <input name="nombre" type="text" placeholder="Nombre" class="input-group-text w-100" required>
                </div>
                <div class="row mt-4 style-label">
                    <label class="mt-2" style="color: black; font-weight: bold; margin-left: 3%;">Apellido *</label>
                </div>
                <div class="row mt-2">
                    <input name="apellido" type="text" placeholder="Apellido" class="input-group-text w-100" required>
                </div>
                <div class="row mt-4 style-label">
                    <label class="mt-2" style="color: black; font-weight: bold; margin-left: 3%;">Telefono</label>
                </div>
                <div class="row mt-2">
                    <input name="telefono" type="number" placeholder="Telefono" class="input-group-text w-100">
                </div>
                <div class="row mt-4 style-label">
                    <label class="mt-2" style="color: black; font-weight: bold; margin-left: 3%;">E-Mail *</label>
                </div>
                <div class="row mt-2">
                    <input name="email" type="email" placeholder="E-Mail" class="input-group-text w-100" required>
                </div>
                <div class="row mt-4 style-label">
                    <label class="mt-2" style="color: black; font-weight: bold; margin-left: 3%;">Mensaje *</label>
                </div>
                <div class="row mt-2">
                    <textarea name="mensaje" class="form-control w-100" rows="5" aria-label="With textarea" placeholder="Escriba su mensaje" id="mensaje" required></textarea>
                </div>
                <div class="row justify-content-center">
                    <input type="submit" value="ENVIAR" name="enviarCorreo" class="botonEnviar">
                </div>

            </form>
        </div>

    </div>
</div>

<?php
    include_once('footer.php');
?>


<?php
if ($mostrar) {
        echo    '<div class="modal" tabindex="-1" role="dialog" id="myModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">'.$titulo.'</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>'.$mensaje.'</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>';
    echo "<script> $('#myModal').modal('show') </script>";
}
?>
<script>
    $(document).on("wheel", "input[type=number]", function (e) {
        $(this).blur();
    });
</script>