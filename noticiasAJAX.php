<?php
    include("admin/configDB.php");
?>
<?php
	if (isset($_GET['pagina'])) {
        $pagina = $_GET['pagina'];
    } else {
        $pagina = 1;
    }
    
    $noticias_x_pagina = 10;
    $offset = ($pagina-1) * $noticias_x_pagina;

    $paginas_totales = "select count(*) from noticias";
    $resultado = mysqli_query($conexion,$paginas_totales);

    $nfilas = mysqli_fetch_array($resultado)[0];
    $paginas_totales = ceil($nfilas / $noticias_x_pagina);

    $instruccion = "select * from noticias order by fecha desc limit ".$offset.",".$noticias_x_pagina.";";

    $consulta = mysqli_query($conexion, $instruccion) or die("Fallo al consultar noticias");
    $nfilas = mysqli_num_rows($consulta);

    if ($nfilas > 0) {
        for ($i=0; $i<$nfilas; $i++) {
            $fila = mysqli_fetch_array($consulta);

            setlocale(LC_TIME, 'es_ES', 'esp_esp'); 
            $año = substr($fila['fecha'],0,4); 
            $mes = substr($fila['fecha'],5,2); 
            $dia = substr($fila['fecha'],8,2);
            
            $mes = ucfirst(strftime("%B", mktime(0, 0, 0, $mes)));

            $fecha = $dia." ".$mes." ".$año;

            print(
                '<div class="post-entry-2 d-flex p-3" style="-webkit-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                -moz-box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);
                box-shadow: -1px 0px 10px 2px rgba(0,0,0,0.15);">
                <div class="thumbnail order-md-2" style="background-image: url('."'images/noticias/".$fila['imagen']."'".')"></div>
                <div class="contents order-md-1 pl-0">
                    <h4><a href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'" style="color: #2c2c2c;">'.$fila['titulo'].'</a></h4>
                    <p class="mb-3">'.substr($fila['cuerpo'], 0, 250).'...</p>
                    <div class="post-meta">
                    <span class="d-block"><a class="color-links" href="ver_noticia.php?id_noticia='.$fila['id_noticia'].'">Ver noticia</a></span>
                    <span class="date-read">'.$fecha.'<span class="mx-1">
                    </div>
                </div>
                </div>'
            );
        }
        
    }
    mysqli_close($conexion);
?>